"""
An API providing basic information about IP networks.
"""

import ipaddress
from typing import Optional
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

app = FastAPI()


class IPNet(BaseModel):
    """
    Network Information
    """

    cidr: str
    net_addr: str
    broadcast_addr: str
    prefix_len: int
    netmask: str
    addr_range: str
    usable_addr: int
    reverse_pointer: Optional[str]


class HTTPError(BaseModel):
    """
    HTTP Error
    """

    detail: str


@app.get(path="/", responses={200: {"model": IPNet}, 400: {"model": HTTPError}})
def get_net_info(q: str) -> IPNet:  # pylint: disable=invalid-name
    """
    Returns basic information about an IP network.
    Supports IPv6 as well as legacy-IP.
    IPv4+ is NOT supported ;)
    """
    try:
        net = ipaddress.ip_network(address=q, strict=False)
    except ValueError as error:
        raise HTTPException(400, str(error)) from error

    # The reverse_pointer attribute for networks is broken / does not make sense,
    # Therefore we include it only if the query is for a single IP address.
    if net.prefixlen == net.max_prefixlen:
        reverse_pointer = net.network_address.reverse_pointer
    else:
        reverse_pointer = None

    return IPNet(
        cidr=f"{net}",
        net_addr=f"{net.network_address}",
        broadcast_addr=f"{net.broadcast_address}",
        prefix_len=f"{net.prefixlen}",
        netmask=f"{net.netmask}",
        addr_range=f"{net.network_address} - {net.broadcast_address}",
        usable_addr=f"{net.num_addresses - 2}",
        reverse_pointer=reverse_pointer,
    )
