# Use python3.10-slim on debian bullseye (from dockerhub) as the base image
FROM python:3.10-slim-bullseye

# Copy the pip requirement into the container
COPY ./requirements/ ./requirements

# Upgrade pip itself
RUN pip install --upgrade pip

# Install the modules needed to run the app - defined in requirements.txt
RUN pip install -r ./requirements/prod.txt

# Copy the contents of 'ip_calc'into the container, into directory '/app'
COPY ./ip_calc /app

# Change the containers working directory (like 'cd /app')
WORKDIR /app

# Expose the containers TCP port 8000 so the webserver can accept incoming connections
EXPOSE 8000

# Start the uvicorn webserver listening on all interfaces on TCP port 8000
CMD ["python", "-m", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
