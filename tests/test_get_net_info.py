import pytest
from fastapi import HTTPException
from ip_calc.main import get_net_info


def test_get_net_info_ipv4_addr() -> None:
    net_info = get_net_info(q="1.1.1.1")
    assert net_info.addr_range == "1.1.1.1 - 1.1.1.1"
    assert net_info.broadcast_addr == "1.1.1.1"
    assert net_info.cidr == "1.1.1.1/32"
    assert net_info.net_addr == "1.1.1.1"
    assert net_info.netmask == "255.255.255.255"
    assert net_info.prefix_len == 32
    assert net_info.usable_addr == -1
    assert net_info.reverse_pointer == "1.1.1.1.in-addr.arpa"


def test_get_net_info_ipv6_addr() -> None:
    net_info = get_net_info(q="ff::")
    assert net_info.addr_range == "ff:: - ff::"
    assert net_info.broadcast_addr == "ff::"
    assert net_info.cidr == "ff::/128"
    assert net_info.net_addr == "ff::"
    assert net_info.netmask == "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff"
    assert net_info.prefix_len == 128
    assert net_info.usable_addr == -1
    assert net_info.reverse_pointer == "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.f.f.0.0.ip6.arpa"


def test_get_net_info_ipv4_net() -> None:
    net_info = get_net_info(q="1.1.1.1/24")
    assert net_info.addr_range == "1.1.1.0 - 1.1.1.255"
    assert net_info.broadcast_addr == "1.1.1.255"
    assert net_info.cidr == "1.1.1.0/24"
    assert net_info.net_addr == "1.1.1.0"
    assert net_info.netmask == "255.255.255.0"
    assert net_info.prefix_len == 24
    assert net_info.usable_addr == 254
    assert net_info.reverse_pointer == None


def test_get_net_info_ipv6_net() -> None:
    net_info = get_net_info(q="ff::/64")
    assert net_info.addr_range == "ff:: - ff::ffff:ffff:ffff:ffff"
    assert net_info.broadcast_addr == "ff::ffff:ffff:ffff:ffff"
    assert net_info.cidr == "ff::/64"
    assert net_info.net_addr == "ff::"
    assert net_info.netmask == "ffff:ffff:ffff:ffff::"
    assert net_info.prefix_len == 64
    assert net_info.usable_addr == 2**64 - 2
    assert net_info.reverse_pointer == None


def test_invalid_input() -> None:
    with pytest.raises(HTTPException):
        get_net_info(q="lkasjdfhasdlfhl")
