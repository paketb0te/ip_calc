# ip_calc

![code style: black](https://img.shields.io/badge/code%20style-black-000000.svg "code style: black")

An API providing basic information about IP networks.

Based on [FastAPI](https://fastapi.tiangolo.com/)
